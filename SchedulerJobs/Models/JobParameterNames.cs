﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerJobs.Models
{
    public static class JobParameterNames
    {
        public static string ScheduleDateTime = "ScheduleDateTime";
        public static string CallbackUrl = "CallbackUrl";
        public static string JobId = "JobId";     
    }
}
