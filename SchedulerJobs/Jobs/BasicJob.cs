﻿using Quartz;
using SchedulerJobs.Helper;
using SchedulerJobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerJobs.Jobs
{
    public class BasicJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            var callbackUrl = dataMap.GetString(JobParameterNames.CallbackUrl);
           // var actionDateTime = dataMap.GetDateTime(JobParameterNames.ActionDateTime);

            string jobId = context.JobDetail.Key.Name;  //jobName=jobId   
                 
            ExpireContent(jobId, callbackUrl);
        }

        private async void ExpireContent(string jobId, string callbackUrl)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    if (!string.IsNullOrWhiteSpace(callbackUrl))
                        callbackUrl = callbackUrl.AddQuery(JobParameterNames.JobId, jobId);

                    // For Get Method
                    var response = await client.GetAsync(callbackUrl);
                    var result = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
