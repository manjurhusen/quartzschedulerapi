﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SchedulerJobs.Helper
{
    public static class QueryStringHelper
    {

        public static string AddQuery(this string url, string name, string value)
        {
            Uri uri = new Uri(url);
            var ub = new UriBuilder(uri);

            // decodes urlencoded pairs from uri.Query to HttpValueCollection
            var httpValueCollection = HttpUtility.ParseQueryString(uri.Query);

            httpValueCollection.Add(name, value);

            // urlencodes the whole HttpValueCollection
            ub.Query = httpValueCollection.ToString();

            return ub.Uri.ToString();
        }

        public static bool IsKeyExistInQueryString(string url,string name)
        {
            if (!string.IsNullOrWhiteSpace(url) && !string.IsNullOrWhiteSpace(name))
            {
                Uri uri = new Uri(url.Trim().ToLower());
                var ub = new UriBuilder(uri);
                var httpValueCollection = HttpUtility.ParseQueryString(uri.Query);
                return httpValueCollection.AllKeys.Contains(name.Trim().ToLower());
            }
            return false;
        }
    }
}
