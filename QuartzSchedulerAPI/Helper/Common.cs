﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace QuartzSchedulerAPI.Helper
{
    public class Common
    {
        public static void SendEmail(string from, string to, string subject, string messageText)
        {
            var message = new MailMessage();
            //From
            message.From = new MailAddress(from);
            //To            
            message.To.Add(to);

            //Subject
            message.Subject = subject;
            //Body
            message.IsBodyHtml = true;
            message.Body = messageText;

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Send(message);
            }

        }

       
    }
}