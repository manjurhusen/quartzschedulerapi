﻿using Quartz;
using QuartzSchedulerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartzSchedulerAPI.Services.Interfaces
{
    public interface ISchedulerService
    {
        IJobDetail GetJobInfo(string jobId);
        string ScheduleJob(JobInfo jobInfo);
        bool RescheduleJob(JobInfo jobInfo);
        bool RemoveJob(string jobId);
        bool IsJobExist(string jobId);
        void SendJobCompleteEmail(string jobId);
    }
}
