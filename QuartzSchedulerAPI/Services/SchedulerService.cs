﻿using Quartz;
using Quartz.Impl;
using QuartzSchedulerAPI.Models;
using QuartzSchedulerAPI.Services.Interfaces;
using SchedulerJobs.Jobs;
using SchedulerJobs.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace QuartzSchedulerAPI.Services
{
    public class SchedulerService : ISchedulerService
    {
       
        public SchedulerService()
        {

        }

        public IJobDetail GetJobInfo(string jobId)
        {
            var jobDetail = App.Scheduler.GetJobDetail(new JobKey(jobId, GetJobGroupName(jobId)));

            return jobDetail;
        }

        public string ScheduleJob(JobInfo jobInfo)
        {
            
            if (jobInfo != null)
            {

                var jobName = Guid.NewGuid().ToString();
                var jobGroupName = GetJobGroupName(jobName);
                var jobTriggerName = GetJobTriggerName(jobName);


                IJobDetail job = JobBuilder.Create<BasicJob>()
                    .WithIdentity(jobName, jobGroupName)
                    .UsingJobData(JobParameterNames.CallbackUrl, jobInfo.CallbackUrl)
                    .UsingJobData(JobParameterNames.ScheduleDateTime, jobInfo.ScheduleDateTime.ToString())
                     .StoreDurably(true)
                       .Build();
                                
                 var timeToFire = GetSchedulerDateTimeOffset(jobInfo.ScheduleDateTime);

                string triggerDescription = "Job (" + jobName + ") Scheduled at DateTime(UTC): " + jobInfo.ScheduleDateTime.ToString();
                ITrigger trigger = TriggerBuilder.Create().WithDescription(triggerDescription)
                  .WithIdentity(jobTriggerName, jobGroupName)
               .StartAt(timeToFire)              
               .ForJob(jobName, jobGroupName)
               .Build();

                App.Scheduler.ScheduleJob(job, trigger);
                return jobName;
            }

            return string.Empty;
        }

        public bool RescheduleJob(JobInfo jobInfo)
        {
            var jobName = jobInfo.JobId;
            var jobGroupName = GetJobGroupName(jobName);
            var jobTriggerName = GetJobTriggerName(jobName);
            var jobKey = new JobKey(jobName, jobGroupName);
            if (App.Scheduler.CheckExists(jobKey))
            {
                var timeToFire = GetSchedulerDateTimeOffset(jobInfo.ScheduleDateTime);
                string triggerDescription = "Job (" + jobName + ") ReScheduled at " + jobInfo.ScheduleDateTime.ToString();

                var triggerKey = new TriggerKey(jobTriggerName, jobGroupName);
                ITrigger existingTrigger = App.Scheduler.GetTrigger(triggerKey);

                if (App.Scheduler.CheckExists(triggerKey))
                {
                    TriggerBuilder tb = existingTrigger.GetTriggerBuilder();

                    ITrigger newTrigger = tb.WithDescription(triggerDescription).StartAt(timeToFire)
                      .ForJob(jobGroupName, jobGroupName)
                      .Build();

                    App.Scheduler.RescheduleJob(existingTrigger.Key, newTrigger);
                }
                else //add new trigger
                {
                    ITrigger trigger = TriggerBuilder.Create().WithDescription(triggerDescription)
                 .WithIdentity(jobTriggerName, jobGroupName)
              .StartAt(timeToFire)
              .ForJob(jobName, jobGroupName)
              .Build();

                   // var job = AppScheduler.Scheduler.GetJobDetail(jobKey);

                    App.Scheduler.ScheduleJob(trigger);
                }

                return true;
            }

            return false;
        }

        public bool RemoveJob(string jobId) //Remove Job and trigger both
        {                
            return App.Scheduler.DeleteJob(new JobKey(jobId, GetJobGroupName(jobId)));              
        }

        public bool IsJobExist(string jobId)
        {           
            return App.Scheduler.CheckExists(new JobKey(jobId, GetJobGroupName(jobId)));
        }

        //public bool UnscheduleJob(string jobId)
        //{
        //    var jobName = jobId;
        //    var jobGroupName = GetJobGroupName(jobId);
        //    var jobKey = new JobKey(jobName, jobGroupName);

        //    if (App.Scheduler.CheckExists(jobKey))
        //    {
        //        return App.Scheduler.UnscheduleJob(new TriggerKey(jobName, jobGroupName));
        //    }
        //    return false;
        //}      


        public void SendJobCompleteEmail(string jobId)
        {
            QuartzSchedulerAPI.Helper.Common.SendEmail("Manjurhusen.Momin@TheNewsMarket.com", "Manjurhusen.Momin@TheNewsMarket.com", "Subject Title - Job Executed For JobId: " + jobId, "Body Content - Job Executed For JobId: " + jobId);
        }


        private string GetJobGroupName(string id)
        {
            return "Group_" + id;
        }
        private string GetJobTriggerName(string id)
        {
            return "Trigger_" + id;
        }

        private DateTimeOffset GetSchedulerDateTimeOffset(DateTime date)
        {
            var minutesToFireDouble = (date - DateTime.UtcNow).TotalMinutes;
            var minutesToFire = (int)Math.Floor(minutesToFireDouble); // negative or zero value will set fire immediately
            return DateBuilder.FutureDate(minutesToFire, IntervalUnit.Minute);
        }
    }

}