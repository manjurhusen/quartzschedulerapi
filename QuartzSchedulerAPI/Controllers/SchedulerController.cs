﻿using Newtonsoft.Json;
using QuartzSchedulerAPI.Models;
using QuartzSchedulerAPI.Services;
using QuartzSchedulerAPI.Services.Interfaces;
using SchedulerJobs.Helper;
using SchedulerJobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TNM.NET45.Logging;

namespace QuartzSchedulerAPI.Controllers
{
    public class SchedulerController : ApiController
    {
        private readonly ISchedulerService schedulerService;
        public SchedulerController(ISchedulerService schedulerService)
        {
            this.schedulerService = schedulerService;
        }

        [HttpGet]
        public IHttpActionResult GetAPIStatus()
        {
            return this.Ok("OK");
        }

        //[HttpGet]
        //public IHttpActionResult StartQuartzService()
        //{
        //    if(!App.Scheduler.IsStarted)  App.Scheduler.Start();
        //    return this.Ok("OK");
        //}

        //[HttpGet]
        //public IHttpActionResult StopQuartzService()
        //{
        //    if (App.Scheduler.IsStarted) App.Scheduler.Shutdown();
        //    return this.Ok("OK");
        //}

        [HttpGet]
        public IHttpActionResult IsQuartzServiceAlive()
        {
            //var result = "InStandbyMode:" + App.Scheduler.InStandbyMode + ", ";
            //result = result + "IsShutdown:" + App.Scheduler.IsShutdown + ", ";
            //result = result + "IsStarted:" + App.Scheduler.IsStarted;
            try
            {
                return this.Ok(App.Scheduler.IsStarted);
            }
            catch
            {
                return this.Ok(false);
            }
            
        }

        [HttpGet]
        public IHttpActionResult GetJobInfo(string id)
        {
            TNMLogger.Instance.Log(string.Format("GetJobInfo : JobId: {0}", id), TNMLogger.LoggerLevelSupported.Info);

            return this.Ok(schedulerService.GetJobInfo(id));
        }

        [HttpPost()]        
        public IHttpActionResult ScheduleJob(JobInfo jobInfo)
        {           
            try
            {
                TNMLogger.Instance.Log(string.Format("ScheduleJob : JobInfo: {0}", JsonConvert.SerializeObject(jobInfo)), TNMLogger.LoggerLevelSupported.Info);

                SchedulerResult result = new SchedulerResult() { Status = false, Message = string.Empty };
                if (ModelState.IsValid)
                {
                    if (QueryStringHelper.IsKeyExistInQueryString(jobInfo.CallbackUrl, JobParameterNames.JobId))
                    {
                        TNMLogger.Instance.Log(string.Format("ScheduleJob : BadRequest CallbackUrl:{0}, ErrorMessage: {1} ",jobInfo.CallbackUrl, Messages.JobIdShouldNotExistInQueryString), TNMLogger.LoggerLevelSupported.Error);
                        return BadRequest(Messages.JobIdShouldNotExistInQueryString);
                    }
                    else
                    {
                        result.JobId = schedulerService.ScheduleJob(jobInfo);
                        if (!string.IsNullOrWhiteSpace(result.JobId))
                        {
                            result.Status = true;
                            result.Message = Messages.JobScheduledSuccessfully;
                        }
                        else result.Message = Messages.JobScheduleFail;                        
                    }

                    TNMLogger.Instance.Log(string.Format("ScheduleJob : Result : {0}", JsonConvert.SerializeObject(result)), TNMLogger.LoggerLevelSupported.Info);
                    return this.Ok(result);
                }
                else
                {
                   TNMLogger.Instance.Log(string.Format("ScheduleJob : BadRequest ErrorMessage:{0}", Messages.ProvideValidRequestData), TNMLogger.LoggerLevelSupported.Error);
                   return BadRequest(Messages.ProvideValidRequestData);
                }
               

            }
            catch(Exception ex)
            {
                TNMLogger.Instance.Log(string.Format("ScheduleJob : Exception : {0}", ex.Message), TNMLogger.LoggerLevelSupported.Error);
                return InternalServerError(new Exception(ex.Message));
            }

           
        }

        
        [HttpPost()]       
        public IHttpActionResult ReScheduleJob(JobInfo jobInfo)
        {
            
            try
            {
                TNMLogger.Instance.Log(string.Format("ReScheduleJob : JobInfo: {0}", JsonConvert.SerializeObject(jobInfo)), TNMLogger.LoggerLevelSupported.Info);

                SchedulerResult result = new SchedulerResult() { Status = false, Message = string.Empty };
               
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrWhiteSpace(jobInfo.JobId))
                    {
                        result.JobId = jobInfo.JobId;
                        if (schedulerService.IsJobExist(jobInfo.JobId)) result.Status = schedulerService.RescheduleJob(jobInfo);
                        else result.Message = Messages.NoJobExist;
                    }
                    else
                    {
                        TNMLogger.Instance.Log(string.Format("ReScheduleJob : BadRequest ErrorMessage:{0}", Messages.JobIdRequiredToReScheduleJob), TNMLogger.LoggerLevelSupported.Error);
                        return BadRequest(Messages.JobIdRequiredToReScheduleJob);
                    }
                }
                else
                {
                    TNMLogger.Instance.Log(string.Format("ReScheduleJob : BadRequest ErrorMessage:{0}", Messages.ProvideValidRequestData), TNMLogger.LoggerLevelSupported.Error);
                    return BadRequest(Messages.ProvideValidRequestData);
                }

                TNMLogger.Instance.Log(string.Format("ReScheduleJob : Result : {0}", JsonConvert.SerializeObject(result)), TNMLogger.LoggerLevelSupported.Info);
                return this.Ok(result);
            }
            catch (Exception ex)
            {
                TNMLogger.Instance.Log(string.Format("ReScheduleJob : Exception : {0}", ex.Message), TNMLogger.LoggerLevelSupported.Error);
                return InternalServerError(new Exception(ex.Message));
            }                  
        }

        [HttpPost()]      
        public IHttpActionResult RemoveJob(string id)
        {                     
            try
            {
                TNMLogger.Instance.Log(string.Format("RemoveJob : jobid: {0}", id), TNMLogger.LoggerLevelSupported.Info);

                SchedulerResult result = new SchedulerResult() { Status = false, Message = string.Empty };
                if (!string.IsNullOrWhiteSpace(id))
                {
                    result.JobId = id;
                    if (schedulerService.IsJobExist(id)) result.Status = schedulerService.RemoveJob(id);
                    else result.Message = Messages.NoJobExist;
                }
                else
                {
                    TNMLogger.Instance.Log(string.Format("RemoveJob : BadRequest ErrorMessage:{0}", Messages.ProvideValidRequestData), TNMLogger.LoggerLevelSupported.Error);
                    return BadRequest(Messages.ProvideValidRequestData);
                }
                TNMLogger.Instance.Log(string.Format("RemoveJob : Result: {0}", JsonConvert.SerializeObject(result)), TNMLogger.LoggerLevelSupported.Info);

                return this.Ok(result);
            }
            catch (Exception ex)
            {               
                TNMLogger.Instance.Log(string.Format("RemoveJob : Exception : {0}", ex.Message), TNMLogger.LoggerLevelSupported.Error);
                return InternalServerError(new Exception(ex.Message));
            }

           
        }


        [HttpGet()]        
        public IHttpActionResult SendJobCompleteEmail(string id)
        {
            schedulerService.SendJobCompleteEmail(id);

            return this.Ok();
        }

        [HttpGet]
        public IHttpActionResult GetUrl(string url)
        {
            url= url.AddQuery("jobid", "45");
            return this.Ok(url);
        }

        [HttpGet]
        public IHttpActionResult IsQueryStringExist(string url,string name)
        {
            var r = QueryStringHelper.IsKeyExistInQueryString(url, name);
            return this.Ok(r);
        }
    }
}
