﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using QuartzSchedulerAPI.Models;
using QuartzSchedulerAPI.Services;
using QuartzSchedulerAPI.Services.Interfaces;
using SDEncoding.Share.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace QuartzSchedulerAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            WindsoreComposition(config);
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void WindsoreComposition(HttpConfiguration config)
        {
            //Initiate Windsor Container
            var container = new WindsorContainer();
            container.Install(new WindsorWebApiInstaller());

            // Register AppSettings
            container.Register(Component.For<SchedulerAppSettings>()
              .DependsOn(Dependency.OnAppSettingsValue("QuartzSchedulerProxyAddress"))
              .DependsOn(Dependency.OnAppSettingsValue("QuartzSchedulerInstanceName"))
              .DependsOn(Dependency.OnAppSettingsValue("QuartzSchedulerProxy"))
              .DependsOn(Dependency.OnAppSettingsValue("QuartzSchedulerThreadCount"))
              );
            // Setup AppSettings           
            App.Prepare(container.Resolve<SchedulerAppSettings>(), container);

            container.Register(Component.For<ISchedulerService>().ImplementedBy<SchedulerService>().LifestyleTransient());
            config.DependencyResolver = new WindsorResolver(container);

        }

        public class WindsorWebApiInstaller : IWindsorInstaller
        {
            /// <summary>
            /// Register API Controllers
            /// </summary>
            /// <param name="container">Windosore container instance.</param>
            /// <param name="store">Http Configuration object.</param>
            public void Install(IWindsorContainer container, IConfigurationStore store)
            {
                container.Register(Classes.FromThisAssembly().BasedOn<ApiController>().LifestyleTransient());
            }
        }
    }
}
