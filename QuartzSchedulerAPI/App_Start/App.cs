﻿using Castle.Windsor;
using Quartz;
using Quartz.Impl;
using QuartzSchedulerAPI.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace QuartzSchedulerAPI
{
    public static class App
    {
        public static IScheduler Scheduler { get; set; }
        public static SchedulerAppSettings AppSettings { get; set; }

        

        public static void Prepare(SchedulerAppSettings appSettings, IWindsorContainer container)
        {
            _container = container;
            AppSettings = appSettings;
            RegisterScheduler();
        }

        private static IWindsorContainer _container;
        public static IWindsorContainer Container
        {
            get { return _container; }
        }

        private static void RegisterScheduler()
        {
            //Address = string.Format("tcp://{0}:{1}/{2}", server, port, scheduler);

            var properties = new NameValueCollection();
            properties["quartz.scheduler.instanceName"] = AppSettings.QuartzSchedulerInstanceName;
            properties["quartz.scheduler.proxy"] = AppSettings.QuartzSchedulerProxy;
            properties["quartz.threadPool.threadCount"] = AppSettings.QuartzSchedulerThreadCount;
            properties["quartz.scheduler.proxy.address"] = AppSettings.QuartzSchedulerProxyAddress;


            var schedulerFactory = new StdSchedulerFactory(properties);

            try
            {
                Scheduler = schedulerFactory.GetScheduler();

                if (!Scheduler.IsStarted)
                    Scheduler.Start();
            }
            catch (SchedulerException ex)
            {
                throw new Exception(string.Format("RegisterScheduler Failed: {0}", ex.Message));
            }
        }




    }
}