﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartzSchedulerAPI.Models
{
	public class SchedulerResult
	{
        public bool Status { get; set; }
        public string Message { get; set; }
        public string JobId { get; set; }
	}
}