﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuartzSchedulerAPI.Models
{
    public class JobInfo
    {
        [Required]
        public DateTime ScheduleDateTime { get; set; }
        [Required]
        public string CallbackUrl { get; set; }
        public string JobId { get; set; }
    }
}