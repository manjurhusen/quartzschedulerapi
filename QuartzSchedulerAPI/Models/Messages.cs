﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartzSchedulerAPI.Models
{
    public class Messages
    {
        public static string JobIdShouldNotExistInQueryString = "jobid should not exist in callbackurl's query string part";
        public static string ProvideValidRequestData = "Please provide valid request data";
        public static string JobIdRequiredToReScheduleJob = "JobId is required to reschedule job";
        public static string NoJobExist = "No job exist for provided jobid";
        public static string JobScheduledSuccessfully = "Job scheduled successfully";
        public static string JobScheduleFail = "Job scheduling process failed";
    }
}