﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartzSchedulerAPI.Models
{
    public class SchedulerAppSettings
    {
        public SchedulerAppSettings(string quartzSchedulerProxyAddress, string quartzSchedulerInstanceName,
            string quartzSchedulerProxy,string quartzSchedulerThreadCount)
        {
            this.QuartzSchedulerProxyAddress = quartzSchedulerProxyAddress;
            this.QuartzSchedulerInstanceName = quartzSchedulerInstanceName;
            this.QuartzSchedulerProxy = quartzSchedulerProxy;
            this.QuartzSchedulerThreadCount = quartzSchedulerThreadCount;
        }

        public string QuartzSchedulerProxyAddress { get; set; }
        public string QuartzSchedulerInstanceName { get; set; }
        public string QuartzSchedulerProxy { get; set; }
        public string QuartzSchedulerThreadCount { get; set; }
    }
}